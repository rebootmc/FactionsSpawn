package xzot1k.plugins.factionsspawn;

import com.massivecraft.factions.Rel;
import com.massivecraft.factions.entity.BoardColl;
import com.massivecraft.factions.entity.Faction;
import com.massivecraft.factions.entity.FactionColl;
import com.massivecraft.factions.entity.MPlayer;
import com.massivecraft.massivecore.ps.PS;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.List;

public class Listeners implements Listener
{
	private FactionsSpawn plugin = FactionsSpawn.getInstance();

	@EventHandler(ignoreCancelled = true)
	public void onJoin(PlayerJoinEvent e)
	{
		MPlayer mplayer = MPlayer.get(e.getPlayer());
		Faction faction = BoardColl.get().getFactionAt(PS.valueOf(e.getPlayer().getLocation()));

		if (mplayer != null && faction != null && !this.passedRelationCheck(mplayer, faction))
		{
			List<String> commands = this.plugin.getConfig().getStringList("spawn-commands");

			for (String command : commands)
			{
				this.plugin.getServer().dispatchCommand(this.plugin.getServer().getConsoleSender(), command.replace("{player}", e.getPlayer().getName()));
			}

			String message = this.plugin.getConfig().getString("spawned-message");

			if (message != null && !message.equalsIgnoreCase(""))
			{
				e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', message));
			}
		}
	}

	private boolean passedRelationCheck(MPlayer mPlayer, Faction faction)
	{
		String comparisonName = faction.getComparisonName();

		if (comparisonName.equals(FactionColl.get().getSafezone().getComparisonName())
				|| comparisonName.equals(FactionColl.get().getWarzone().getComparisonName())
				|| comparisonName.equals(FactionColl.get().getNone().getComparisonName()))
		{
			return true;
		}

		Rel          relation = mPlayer.getRelationTo(faction);
		List<String> list     = this.plugin.getConfig().getStringList("ignored-relations");

		for (String rel : list)
		{
			if (rel.equalsIgnoreCase("own") && mPlayer.getFaction().getName().equalsIgnoreCase(faction.getName()))
			{
				return true;
			}

			if (!relation.name().equalsIgnoreCase(rel.toUpperCase().replace(" ", "_").replace("-", "_")))
			{
				continue;
			}

			return true;
		}

		return false;
	}

}

