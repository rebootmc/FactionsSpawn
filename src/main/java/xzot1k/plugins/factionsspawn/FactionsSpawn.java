package xzot1k.plugins.factionsspawn;

import org.bukkit.plugin.java.JavaPlugin;

public final class FactionsSpawn extends JavaPlugin
{
    private static FactionsSpawn instance;

    public static FactionsSpawn getInstance()
    {
        return instance;
    }

    public static void setInstance(FactionsSpawn instance)
    {
        FactionsSpawn.instance = instance;
    }

    @Override
    public void onEnable()
    {
        FactionsSpawn.setInstance(this);
        this.saveDefaultConfig();
        this.getServer().getPluginManager().registerEvents(new Listeners(), this);
    }
}

